import React, {Component} from 'react';
import {Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Button, Modal, ModalHeader, ModalBody, FormGroup, Label } from 'reactstrap';
import {Link} from 'react-router-dom';
import {Control, LocalForm, Errors} from 'react-redux-form';
import {Loading} from './LoadingComponent';
import {baseUrl} from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const required = (val)=> val && val.length;
const maxLength = (len)=>(val)=> !(val) || (val.length <= len);
const minLength = (len)=>(val)=> (val) && (val.length >= len);

class CommentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  handleSubmit(values) {
    console.log('Current State is: ' + JSON.stringify(values));
    this.props.postComment(this.props.dishId, values.rating, values.name, values.comment);
  }

  render() {
    return(
      <div>
        <Button outline onClick={this.toggleModal}><span className="fa fa-comment"></span> Add Comment</Button>

        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
          <ModalBody>
            <LocalForm onSubmit={(values)=> this.handleSubmit(values)}>
              <FormGroup>
                <Label className="mr-3 font-weight-bold" htmlFor="rating">Rating:</Label>
                <Control.select model=".rating" name="rating"
                        className="form-cotrol" validators={{required}}>
                    <option></option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </Control.select>
                <Errors
                  className="text-danger font-weight-bold"
                  model=".rating"
                  show="touched"
                  messages={{
                    required: '**Select a rating between 1 and 5 '
                  }}
                />
              </FormGroup>
              <FormGroup>
                <Label className="font-weight-bold" htmlFor="rating">Name:</Label>
                <Control.text model=".name" id="name" name="name"
                    placeholder="Name"
                    className="form-control"
                    validators={{
                      required, minLength: minLength(3), maxLength: maxLength(15)
                    }}
                  />
                <Errors
                  className="text-danger font-weight-bold"
                  model=".name"
                  show="touched"
                  messages={{
                    required: '**Required.. ',
                    minLength: '**Must be > 2 characters.. ',
                    maxLength: '**Must be < 15 characters.. '
                  }}
                />
              </FormGroup>
              <FormGroup>
              <Label className="font-weight-bold" htmlFor="message">Comment:</Label>
                <Control.textarea model=".comment" id="comment" name="comment" placeholder="Your Comment Here..."
                  className="form-control"
                  validators = {{
                    required, minLength: minLength(10), maxLength: maxLength(120)
                  }}
                />
                <Errors
                  className="text-danger font-weight-bold"
                  model=".comment"
                  show="touched"
                  messages={{
                    required: '**Required.. ',
                    minLength: '**Must be at least 10 characters.. ',
                    maxLength: '**Must be maximum of 120 characters.. '
                  }}
                />
              </FormGroup>
              <FormGroup>
                <Button type="submit" value="submit" color="primary">Submit</Button>
              </FormGroup>
            </LocalForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

function RenderDish({dish}){
    if(dish != null){
        return(
            <div className='col-12 col-md-5 m-1'>
              <FadeTransform in
                transformProps={{
                  exitTransform: 'scale(0.5) translateY(-50%)'
                }}>
                <Card>
                    <CardImg width="100%" object src={baseUrl + dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle className="font-weight-bold">{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
              </FadeTransform>
            </div>
        );
    }
    else{
        return(
            <div></div>
        );
    }
}

function RenderComments({comments, postComment, dishId}){
    if(comments != null){
        const com = comments.map((comment)=>{
            return(
              <Fade in>
                <li key={comment.id}>
                    <p className='mb-2'>{comment.comment}</p>
                    <p className='mb-2'>-- {comment.author} , {comment.date}</p>

                </li>
              </Fade>
            );
        })
        return(
            <div className='col-12 col-md-5 m-1'>
                <div>
                    <h4 className='mb-4'>Comments</h4>
                    <ul className='list-unstyled'>
                      <Stagger in>
                        {com}
                      </Stagger>
                    </ul>
                </div>

                <CommentForm dishId={dishId} postComment={postComment} />

            </div>
        );
    }
    else{
        return(
            <div>
              <CommentForm />
            </div>
        );
    }
}

const DishDetail = (props)=>{
  if(props.isLoading){
    return(
      <div className="container">
        <div className="row">
          <Loading />
        </div>
      </div>
    );
  }
  else if(props.errMess){
    return(
      <div className="container">
        <div className="row">
          <h4>{props.errMess}</h4>
        </div>
      </div>
    );
  }
  else if(props.dish != null){
    return(
    <div className='container'>
      <div className="row">
        <Breadcrumb>
          <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
          <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
        </Breadcrumb>
        <div className="col-12">
          <h3>{props.dish.name}</h3>
          <hr />
        </div>
      </div>
      <div className='row'>
          <RenderDish dish = {props.dish} />
          <RenderComments comments = {props.comments} postComment={props.postComment}
        dishId={props.dish.id} />
      </div>
    </div>
    );
  }
  else{
    return(
      <div></div>
    );
  }

}

export default DishDetail;
